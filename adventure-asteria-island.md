# Adventure Asteria Island

- [ ] 19th day, Blackburn attacks New Shadeport
    - [ ] 16th day, Blackburn moves out
    - [x] 15th day, Blackburn create camp outside of Blackburn in prep

- [x] Fang & Ji join Blackburn to find Jerd
    - [x] Jerd joined to be a priest

- [x] Rebels living under blackburn in tunnel system
    - Rebels are overly bold & direct
        - Anything less isn't impactful enough
            - Not enough time, so put it all on the line now
            - Nothing to lose
            - Too late

- [x] Fang & Ji reveal themselves on the 16th day (healthy)
    - They wrote the letter from "Mike" and joined blackburn
    - They are unrecogonizable, renamed themselves
        - Fang is Hons, Ji is Si

- Ends when players does 1 of the following:
    - Reach the mainland
    - Obtain Hecate's Dagger and keeps or destorys it
    - Joins blackburn and destorys New Shadeport
    - Throws Blackburn into turmoil

