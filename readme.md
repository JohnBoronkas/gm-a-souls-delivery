# Readme

Create directories for each hex grid. Create sub-directories for each hex and optionally for each location within the hex depending on the complexity of the area.

Each directory may contain a top-level "00-" file that is for overall information for that area.

Populate individual files with locations, npcs, and monsters.

## File Prefixes

- loc: location
- npc: npc
- mob: monster

