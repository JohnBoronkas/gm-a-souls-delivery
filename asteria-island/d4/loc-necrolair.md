# Necrolair

- maze of flesh, blood, shifting walls, and directional wind that leads to a large chamber of screams & shadows dancing on the walls with a bubbling pit of black tar in the middle
    - players need to follow the in and out breathing of the wind to find their way to or from the central chamber, when walls shift the winds shift with it
- bodies on flesh alters, black priests chanting low hyms of unknown faith over them
- zombies and skeletons (that need to have their bodies burned or bones broken to be killed) roam the flesh halls

- Tarathra, Sovereign of the Black lives at the bottom of the tar pit that is mostly (80%) drained, she is only partly submerged at this point, she has to duck or laydown to stay under.
    - She holds Hecate's Dagger (hec-ate) & controls the zombies. If found, she taunts the players as zombies emerge from the pool & the shadows on the walls circling the room cry out for help, in clear pain and despair.
- In a direct fight, Tarathra swaps the souls of the players swap characters, your personality and control in the mind and body of another (or as they run if they stick around too long, Tarathra tries to hold them there)
    - 2 priests defend her with their magic, need to kill the priests to break their spell defending her. Zombies swarm, priests defend, Tarathra attacks when safe or uses magic to escape and tries to hide under the tar in whats left of the pool.
        - Priests raise from the pool with their unlit lanterns -- they each suck a soul from the walls to light their purple lanterns
        - After a turn of being dead, a new priest raises from the pool (players need to kill both priests to make her vulnerable to attacks)

