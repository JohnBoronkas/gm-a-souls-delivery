# Tarathra, Sovereign of the Black

- Weak from all the magic corruption damage done to her body

str: 3
dex: 8
wil: 14

S: Hatred (turn target against friends)
S: Smoke Form (turn to smoke, fast, & immune to mundane)
S: Smoke Arrow (d6, ignore 1 armor)
S: Soul Swap (swap nearby souls to new bodies)

2 hp

