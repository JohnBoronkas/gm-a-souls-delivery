# Blackburn

## Overall details

- A small city, rooftops, bustling streets
- Ran by a city councle seated by local
    - Includes allied mainland nations who rotate for advisory
- Blackburn feels like they already won
    - Mainland too politically tangled to do anything about their zombies
    - Some mainlanders aware, but can't get support to do anything about it
- Working in the city requires joining blackburn
- If outsider is not in black & not handsome af:
    - Guards push for outsiders to join, highlighting benefits
        - 2 hour lunch breaks
        - Immortality
        - Healthy pension
    - Initial ritual of plastic surgery, 3 days recovery
        - [ ] Will find and want to remove Anacard's sprout
    - Personal shopper for their wardrobe
    - Complemetry makeup bag (holds 10 pieces of minor makeup supplies)
    - Complemetry tube of eyeliner
- [x] 3 massive ships in docks
