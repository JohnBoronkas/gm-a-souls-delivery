# The Anchored Barrel (Tavern & Inn)

- Ran by Felix
- Salt air, ale, sizzling meats, dim lighting from shell encased candels
- Sailors, merchants, shady figures, lute music, whispered conversations
Buzzing activity with undercurrent of unspoken secrets
- Vacant table near hearth with carved anchor (haunted)

