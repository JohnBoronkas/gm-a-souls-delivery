# Wavehammer (Blacksmith & weapon merchant)

- Ran by Kylie
- Rhythmic clang of metal, flying sparks, thick air scented with molten metal & coal
- Tangible heat, raw but meticulous setting
- Highly focused blacksmith, fleeting glance at a hidden compartment under counter
- Tools in alignment, each weapon in its place

