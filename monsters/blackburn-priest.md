# Blackburn Priest

6 hp (2 armor)

- Comes every hour for dead bodies within range of their home
    - Drags them back to their flesh cave to convert the body into zombie
- Casts spells
    - Uses small metal cage attached to chain to focus spells
        - Burns ingredients inside of it to power the unique spells
            - Players can't use unless they learn how to cast that magic
                - Can become a priest with training from Blackburn
    - Purple UV light style effects, uses wisps to find & collect body parts
    - Needler style explosive spells
- Wears human flesh died black with thorns
    - When killed, the flesh cloak and thorns go with it as body shatters
        - Leaves metal cage behind
- Defends by making purple crystal clones of self that are illusions
    - Shatter when hit

