# Scratch

Weather: 2

## Datetime

87th Sun
2nd Moon
16th Day

0800 (rest)
0810

## Notes

Players planning to use 1 rebel group to secure towers and surrounding area, take rest into the caves with them.

PICKUP The rebel invasion of the flesh mound! Picking up directly after player's long rest on the day Blackburn is moving out against New Shadeport



- Rebel encounter
- Restaurant has wine barrels, butcher around corner near stairs, and cooking supplies
    - 2nd to last wine barrel is fake and is a small room that leads down to the rebel caves
- In cave of Restaurant (5 foot high ceiling, cramped area), left side is heavy door with single rope pull and lots of sound-proof padding that leads to rebel area and underground cave system that connects the city (players will get questioned)
    - right side is a 10 foot long, 10 foot deep pit with a single treasure chest on the other side (unlocked, and full of sleeping gas). The pit is full of quicksand.

